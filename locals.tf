# ws@2023 locals.tf

locals {

  create = var.create ? true : false
  
  name   = var.name

  tags = {
    terraform = "true"
  }
}

