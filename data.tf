# ws@2023 data.tf

data "aws_region" "default" {}

data "aws_canonical_user_id" "default" {}

data "aws_caller_identity" "default" {}

data "aws_partition" "default" {}

# Fetching all availability zones [AZS]
data "aws_availability_zones" "default" {}

# See:
# * https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_examples_aws_my-sec-creds-self-manage.html

data "aws_iam_policy_document" "default" {

  count = local.create ? 1 : 0

  statement {

    sid = "AllowViewAccountInfo"

    effect = "Allow"

    actions = [
      "iam:GetAccountPasswordPolicy",
      "iam:GetAccountSummary",
      "iam:ListAccountAliases",
      "iam:ListGroups",
      "iam:ListVirtualMFADevices",
      "iam:ListUsers"
    ]

    resources = ["*"]
  }

  statement {

    sid = "AllowManageOwnPasswords"

    effect = "Allow"

    actions = [
      "iam:CreateLoginProfile",
      "iam:ChangePassword",
      "iam:DeleteLoginProfile",
      "iam:GetLoginProfile",
      "iam:GetUser",
      "iam:UpdateLoginProfile"
    ]

    resources = [
      "arn:aws:iam::${data.aws_caller_identity.default.account_id}:user/&{aws:username}"
    ]
  }

  statement {
    sid = "AllowManageOwnAccessKeys"

    effect = "Allow"

    actions = [
      "iam:CreateAccessKey",
      "iam:DeleteAccessKey",
      "iam:ListAccessKeys",
      "iam:UpdateAccessKey"
    ]

    resources = [
      "arn:aws:iam::${data.aws_caller_identity.default.account_id}:user/&{aws:username}"
    ]
  }

  statement {

    sid = "AllowManageOwnSigningCertificates"

    effect = "Allow"

    actions = [
      "iam:DeleteSigningCertificate",
      "iam:ListSigningCertificates",
      "iam:UpdateSigningCertificate",
      "iam:UploadSigningCertificate"
    ]

    resources = [
      "arn:aws:iam::${data.aws_caller_identity.default.account_id}:user/&{aws:username}"
    ]
  }

  statement {

    sid = "AllowManageOwnSSHPublicKeys"

    effect = "Allow"

    actions = [
      "iam:DeleteSSHPublicKey",
      "iam:GetSSHPublicKey",
      "iam:ListSSHPublicKeys",
      "iam:UpdateSSHPublicKey",
      "iam:UploadSSHPublicKey"
    ]

    resources = [
      "arn:aws:iam::${data.aws_caller_identity.default.account_id}:user/&{aws:username}"
    ]
  }

  statement {
    sid = "AllowManageOwnGitCredentials"
    effect = "Allow"
    actions = [
      "iam:CreateServiceSpecificCredential",
      "iam:DeleteServiceSpecificCredential",
      "iam:ListServiceSpecificCredentials",
      "iam:ResetServiceSpecificCredential",
      "iam:UpdateServiceSpecificCredential"
    ]
    resources = [
      "arn:aws:iam::${data.aws_caller_identity.default.account_id}:user/&{aws:username}"
    ]
  }

  statement {

    sid = "AllowManageOwnVirtualMFADevice"

    effect = "Allow"

    actions = [
      "iam:CreateVirtualMFADevice",
      "iam:DeleteVirtualMFADevice",
      "iam:ListVirtualMFADevices"
    ]
    
    resources = [
      "arn:aws:iam::${data.aws_caller_identity.default.account_id}:mfa/*",
      "arn:aws:iam::${data.aws_caller_identity.default.account_id}:mfa/&{aws:username}"
    ]
  }

  statement {

    sid = "AllowManageOwnUserMFA"
    
    effect = "Allow"

    actions = [
      "iam:DeactivateMFADevice",
      "iam:EnableMFADevice",
      "iam:ListMFADevices",
      "iam:ResyncMFADevice"
    ]

    resources = [
      "arn:aws:iam::${data.aws_caller_identity.default.account_id}:mfa/*",
      "arn:aws:iam::${data.aws_caller_identity.default.account_id}:user/&{aws:username}"
    ]
  }

  statement {

    sid = "DenyAllExceptListedIfNoMFA"
    
    effect = "Deny"
    
    not_actions = [
      "iam:ChangePassword",
      "iam:CreateLoginProfile",
      "iam:CreateVirtualMFADevice",
      "iam:EnableMFADevice",
      "iam:GetUser",
      "iam:ListAccountAliases",
      "iam:ListGroups",
      "iam:ListMFADevices",
      "iam:ListUsers",
      "iam:ListVirtualMFADevices",
      "iam:ResyncMFADevice",
      "sts:GetSessionToken"
    ]
    
    resources = ["*"]
    
    condition {
      test     = "BoolIfExists"
      variable = "aws:MultiFactorAuthPresent"
      values = [
        "false"
      ]
    }
  }
}

