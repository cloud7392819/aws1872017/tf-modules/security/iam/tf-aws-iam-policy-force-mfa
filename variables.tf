# ws@2023 variables.tf

variable "name" {
  description = "The name of your Policy"
  default     = null
}

variable "create" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default     = false
}

variable "description" {
  description = "Description"
  default     = null
}

