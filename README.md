# tf-aws-iam-policy-force-mfa

========================

Creates IAM policy forces users to use MFA.

## Usage

```hcl-terraform

module "iam_policy_force_mfa" {

  source = "git::https://gitlab.com/cloud7392819/aws1872017/tf-modules/iam/tf-aws-iam-policy-force-mfa.git?ref=tags/x.x.x"

  create = true

  name = "${var.cut_name}-ForceMFAPolicy"
  
  description = "This policy allows users to manage their own passwords and MFA devices but nothing else unless they are authenticated with MFA."
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-------:|:--------:|
| create | The Whether of the policy. | bool | `false` | no |
| name | The Name of the policy. | string | null | yes |
| description | The Description of the policy. | string | null | yes |

## Outputs

| Name | Description |
|------|-------------|
| iam_policy_id | The policy's ID |
| iam_policy_arn | The ARN assigned by AWS to this policy |
| iam_policy_name | The name of the policy |


